import { useDisclosure, useToast } from "@chakra-ui/react";
import { useState } from "react";
import appointmentApi from "../api/appointmentApi";
import { IAppointments, IPatient } from "../interfaces/IPatient";
import { useForm } from "./useForm";


const initialStateReset = { 
    name: '',
    lastname: '',
    birthday: new Date().toISOString().split('T')[0],
    blood: '',
    weight: 0,
    size_birth: 0,
    head_circunference: 0,
    a_pagar: 0,
    sex: '',
    eg: 0,
    motherName: '',
    fatherName: '',
    motherPhone: '',
    fatherPhone:''
}



export const usePatient = () => {

    const toast = useToast();
    const [ isSearching, setIsSearching ] = useState(false);
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [ appointments, setAppointments ] = useState<IAppointments[]>([])
    const [ initialState ] = useState<IPatient>({
      name: '',
      lastname: '',
      birthday: new Date().toISOString().split('T')[0],
      blood: '',
      weight: 0,
      size_birth: 0,
      head_circunference: 0,
      a_pagar: 0,
      sex: '',
      eg: 0,
      motherName: '',
      fatherName: '',
      motherPhone: '',
      fatherPhone:''
    })
    const { onChange, form, setState } = useForm(initialState)
  
    const onSetInitialState = ( patient: IPatient) => {
      patient.birthday = new Date(patient.birthday).toISOString().split('T')[0];
      setState( patient);
    }
  
    const onFormReset = () => {
      setState(initialStateReset);
      setIsSearching(false);
      setAppointments([]);
      toast({
        title: 'Formulario reseteado.',
        description: `Formulario limpio.`,
        status: 'info',
        duration: 5000,
        isClosable: true,
      })
    }
  
    const onSearchingChange = () => {
      setIsSearching(true);
    }
  
    // console.log(form)
  
    const loadAppointments = async(id: string) => {
      try {
        const { data } = await appointmentApi.get<IAppointments[]>(`/appointment/${id}`);
        console.log(data)
        setAppointments(data)
      } catch (error: any) {
        toast({
          title: 'Error.',
          description: error.response.data.msg,
          status: 'error',
          duration: 5000,
          isClosable: true,
        })
      }
    }
  
    const onSubmit = async() => {
      try {
        const { data } = await appointmentApi.post<IPatient>('/patient', form);
        toast({
          title: 'Paciente Registrado.',
          description: `Paciente ${data.name} registrado.`,
          status: 'success',
          duration: 5000,
          isClosable: true,
        })
      return;
      } catch (error:any) {
        toast({
          title: 'Error.',
          description: error.response.data.msg,
          status: 'error',
          duration: 5000,
          isClosable: true,
        })
      }
    
    }

    

    return {
        form,
        onChange,
        isSearching,
        appointments,
        onSetInitialState,
        onFormReset,
        onSearchingChange,
        loadAppointments,
        onSubmit,
        isOpen, 
        onOpen, 
        onClose, 



    }
}
