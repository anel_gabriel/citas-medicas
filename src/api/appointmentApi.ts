import axios from "axios"


const baseURL = import.meta.env.VITE_BASEURL;



const appointmentApi = axios.create({ baseURL });


appointmentApi.interceptors.request.use(
    async ( config ) => {
        return config;
    }
)


export default appointmentApi;