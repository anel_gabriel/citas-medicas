import { Accordion, AccordionItem, AccordionButton, AccordionIcon, AccordionPanel, HStack, Box, Text } from "@chakra-ui/react"
import { FC } from 'react';
import { IAppointments } from "../interfaces/IPatient";

interface Props {
    patient: IAppointments

}

export const Citas:FC<Props> = ({ patient }) => {

    const { age, head_circunference, weight, size, createdAt } = patient;

    return (
        <Accordion allowToggle my={2}>
            <AccordionItem>
            <h2 >
                <AccordionButton bgColor='#31608C' px={5} w='95%' ml={2} _hover={{ opacity: 0.9,  }} >
                <Box as="span"  fontWeight='semibold' flex='1' textAlign='left' color='#fff' >
                    Cita
                </Box>
                <Text color='#fff' fontWeight='semibold'>{ new Intl.DateTimeFormat("en-GB").format(new Date(createdAt))  }</Text>
                <AccordionIcon color='#fff' />
                </AccordionButton>
            </h2>
            <AccordionPanel 
                pb={4} 
                bgColor='#5AB0FF' 
                px={5} 
                w='95%' 
                ml={2} 
                borderBottomRightRadius={10}
                borderBottomLeftRadius={10}
            >
                <Box >
                <HStack justifyContent='space-around'>
                    <Text color='#fff'>Edad: {age} Meses</Text>
                    <Text color='#fff'>Talla: {size}cm</Text>
                </HStack>
                <HStack justifyContent='space-around'>
                    <Text color='#fff'>Peso: {weight}L</Text>
                    <Text color='#fff'>P.C: {head_circunference}cm</Text>
                    
                </HStack>
                </Box>
            </AccordionPanel>
            </AccordionItem>
        </Accordion>
    )
}
