import { FormEvent, useRef, FC } from 'react';
import { AddIcon } from "@chakra-ui/icons"
import { HStack, IconButton, Box, Text, Button, FormControl, FormLabel, GridItem,  Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, SimpleGrid, useDisclosure, InputGroup, InputLeftAddon, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, useToast } from "@chakra-ui/react"
import { Citas } from "../components"
import { useForm } from "../hooks/useForm"
import appointmentApi from "../api/appointmentApi"
import { IAppointments, IPatient } from '../interfaces/IPatient';


interface Props {
    patientId?: string;
    appoitments: IAppointments[];
    loadAppointments: (id: string) => Promise<void>;
}


export const Historial: FC<Props> = ({ appoitments, loadAppointments, patientId }) => {

    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast();

    const { onChange, form } = useForm({
        patientId:"",
        age: "",
        size: "",
        weight: "",
        head_circunference: "",
    })
    const initialRef = useRef(null)
    const finalRef = useRef(null)

    
  const onSubmit = async(e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    form.patientId = patientId || '';
    // console.log(form)
    try {
      const { data } = await appointmentApi.post<IPatient>('/appointment', form);
      console.log(data)
      loadAppointments(form.patientId);
      onClose();

      toast({
        title: 'Cita Registrada.',
        description: `Seguimiento registrado.`,
        status: 'success',
        duration: 5000,
        isClosable: true,
    })
    return;
    } catch (error:any) {
      toast({
        title: 'Error.',
        description: error.response.data.msg,
        status: 'error',
        duration: 5000,
        isClosable: true,
      })
    }
  
  }

    return (
        <>
            <Box w='28%' h='470px' bgColor='#F5F5F5' borderRadius={8} overflowY='scroll' >
                <HStack justifyContent='space-between' pt={2} px={3}>
                <Text color='#31608C' fontWeight='semibold' fontSize='lg'>Historial</Text>

                {/* {
                    appoitments.length > 0 &&
                    ( */}
                        <IconButton
                            variant='solid'
                            colorScheme="blue"
                            onClick={onOpen}
                            borderRadius={30}
                            aria-label='Call Sage'
                            fontSize='13px'
                            icon={<AddIcon  color='#fff'/>}
                        />
                    {/* )
                }
            */}
                </HStack>
                <Box pt={6} >
                    {
                        appoitments.map( ap => (
                            <Citas  
                                key={ap._id}
                                patient={ ap }
                            />

                        ))
                    }
                </Box>

            </Box>
            <Modal
                initialFocusRef={initialRef}
                finalFocusRef={finalRef}
                scrollBehavior="outside"
                isOpen={isOpen}
                onClose={onClose}
            >
            <ModalOverlay />
            <ModalContent>
                <form onSubmit={ onSubmit }>
                    <ModalHeader>Registrar Cita</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody pb={6}>
                        <SimpleGrid columns={2} columnGap={4} rowGap={3}  w='full' p={2}  >
                                <GridItem colSpan={1} >
                                <FormControl>
                                    <FormLabel color='#31608C'>Edad</FormLabel>
                                    <NumberInput 
                                        defaultValue={0} 
                                        value={form.age}
                                        precision={1} 
                                        step={1}
                                        onChange={ (e) => onChange(e, 'age')}
                                        // border='1px' 
                                    >
                                        <NumberInputField border='1px' borderColor='gray.500'  />
                                        <NumberInputStepper>
                                            <NumberIncrementStepper />
                                            <NumberDecrementStepper />
                                        </NumberInputStepper>
                                    </NumberInput>
                                </FormControl>
                                </GridItem>   
                                <GridItem colSpan={1} >
                                    <FormControl>
                                        <FormLabel color='#31608C'>Talla</FormLabel>
                                        <InputGroup>
                                        <InputLeftAddon children='cm' bgColor='gray.400' />
                                        <NumberInput 
                                            // defaultValue={0} 
                                            value={form.size}
                                            precision={2} 
                                            step={0.1}
                                            onChange={ (e) => onChange(e, 'size')}
                                            // border='1px' 
                                        >
                                            <NumberInputField border='1px' borderColor='gray.500'  />
                                            <NumberInputStepper>
                                                <NumberIncrementStepper />
                                                <NumberDecrementStepper />
                                            </NumberInputStepper>
                                        </NumberInput>
                                        </InputGroup>
                                    </FormControl>
                                </GridItem>   
                                <GridItem colSpan={1} >
                                <FormControl>
                                    <FormLabel color='#31608C'>Peso</FormLabel>
                                    <InputGroup>
                                    <InputLeftAddon children='Lbs' bgColor='gray.400' />
                                    <NumberInput 
                                        // defaultValue={0} 
                                        value={form.weight}
                                        precision={1} 
                                        step={1}
                                        onChange={ (e) => onChange(e, 'weight')}
                                        // border='1px' 
                                    >
                                        <NumberInputField border='1px' borderColor='gray.500'  />
                                        <NumberInputStepper>
                                        <NumberIncrementStepper />
                                        <NumberDecrementStepper />
                                        </NumberInputStepper>
                                    </NumberInput>
                                    </InputGroup>
                                </FormControl>
                                </GridItem>  
                                <GridItem colSpan={1} >
                                <FormControl>
                                    <FormLabel color='#31608C'>Perimetro Cefalico</FormLabel>
                                    <InputGroup>
                                    <InputLeftAddon children='cm' bgColor='gray.400'  />
                                        <NumberInput 
                                            // defaultValue={0} 
                                            value={form.head_circunference}
                                            precision={2} 
                                            step={0.1}
                                            onChange={ (e) => onChange(e, 'head_circunference')}
                                            // border='1px' 
                                        >
                                            <NumberInputField border='1px' borderColor='gray.500'  />
                                            <NumberInputStepper>
                                                <NumberIncrementStepper />
                                                <NumberDecrementStepper />
                                            </NumberInputStepper>
                                    </NumberInput>
                                    </InputGroup>
                                </FormControl>
                                </GridItem>    
                        </SimpleGrid>


                    </ModalBody>

                    <ModalFooter>
                        <Button colorScheme='blue' mr={3} type="submit">
                            Guardar
                        </Button>
                        <Button onClick={onClose}>Cancelar</Button>
                    </ModalFooter>
                </form>
            </ModalContent>
            </Modal> 
        </>
    )
}
