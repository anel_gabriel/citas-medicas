import { FC } from 'react';
import {  Text, Flex, HStack, Avatar, Button } from "@chakra-ui/react";
import { ChakraStylesConfig } from "chakra-react-select";
import { AsyncSelect } from 'chakra-react-select';
import appointmentApi from "../api/appointmentApi";
import { IPatient } from "../interfaces/IPatient";
import { RepeatIcon } from '@chakra-ui/icons';
import Foto from '../assets/foto.jpeg';

interface Options {
    value: IPatient;
    label: string;
}

const options = [
    { value:{}, label: 'Anel' },
]


interface Props {
    setInitialState: (patient: IPatient) => void;
    onFormReset: () =>  void;
    onSearchingChange: () => void;
    loadAppoinments: ( id: string ) => Promise<void>
}

export const AppBar:FC<Props> = ({ setInitialState, onFormReset, onSearchingChange, loadAppoinments }) => {

    const chakraStyles: ChakraStylesConfig = {
        container: ( provided) => ({
            ...provided,
            backgroundColor: 'white',
            borderColor: 'gray.500',
            width: '40%',
            borderRadius: 30,
        }),
        control: ( provided ) => ({
            ...provided,
            borderRadius: 20
        })
    };

    const searchPatient = async(inputValue: string) => {
        const { data } = await appointmentApi.get<IPatient[]>(`/patient/${inputValue}`);
        return data.map( (p) => ({
            label: `${p.name} ${p.lastname}`,
            value: p
        }) )
    };

    const promiseOptions = (inputValue: string) =>
        new Promise<Options[]>((resolve) => {
            setTimeout(() => {
            resolve(searchPatient(inputValue));
        }, 1000);
    });


    const onSelectChange = (e: any) => {
        onSearchingChange();
        setInitialState(e.value);
        loadAppoinments(e.value._id);
    }
      
    return (
        <Flex justifyContent='space-between' pt={3}  >
            <HStack  flex='1' justifyContent='center'>
                <AsyncSelect 
                    placeholder="Buscar"
                    options={ options } 
                    chakraStyles={chakraStyles}
                    loadOptions={promiseOptions}
                    onChange={ onSelectChange}
                />
                <Button onClick={onFormReset}  leftIcon={<RepeatIcon />} borderRadius={20} colorScheme="blue">Limpiar</Button>
            </HStack>
            <HStack >
                <Avatar name='Anel Dominguez' src={Foto} />
                <Text  color='#31608C' fontWeight='semibold'>Dr. Angel De La Paz</Text>
            </HStack>
        </Flex>
    )
}
