import { Container, Flex, HStack, Input, Box, Heading, VStack, SimpleGrid, FormControl, FormLabel, GridItem,  Button, Select, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, InputGroup, InputLeftAddon } from "@chakra-ui/react"
import { AppBar, Historial } from "./layout"
import './App.css';

import { usePatient } from "./hooks/usePatient";


const App = () => {

  const { form, onChange, onSubmit, onSearchingChange, onSetInitialState, 
    onFormReset, loadAppointments, appointments, isSearching } = usePatient();

  return (
    <>
      <Container bgColor='#E2E2E2' w='5xl' maxW='container.xl'  maxH='container.xl'  h='800px' >
      {/* Appbar */}
        <AppBar 
          onSearchingChange={ onSearchingChange } 
          setInitialState={ onSetInitialState } 
          onFormReset={onFormReset} 
          loadAppoinments={ loadAppointments }
        />
        <Flex>
          <Heading size='lg' fontWeight='normal' color='#31608C'>Datos del Paciente</Heading>
        </Flex>
        {/* <form onSubmit={ onSubmit }> */}
          <Box w='100%' >
            <HStack justifyContent='space-between' gap={2} wrap='wrap'>
              <Box w='70%' h='470px' bgColor='#F5F5F5' borderRadius={8}>
                <VStack w="full" h="full" pb={0} px={5}  spacing={3} alignItems="flex-start">
                  <SimpleGrid columns={2} columnGap={3} rowGap={4} w='full' px={2} pt={5} gridColumnGap={50}>
                    <GridItem colSpan={1} >
                      <FormControl>
                          <FormLabel color='#31608C'>Nombre</FormLabel>
                          <Input 
                            placeholder=''
                            variant='outline' 
                            border='1px' 
                            borderColor='gray.500'
                            name="name"
                            value={ form.name }
                            onChange={ ({ target }) => onChange(target.value, 'name') } 
                            autoComplete="off"
                          />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>Apellido</FormLabel>
                          <Input 
                            placeholder=''
                            variant='outline' 
                            border='1px' 
                            borderColor='gray.500'
                            name="surname"
                            value={ form.lastname }
                            onChange={ ({ target }) => onChange(target.value, 'lastname') } 
                            autoComplete="off"
                          />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>Fecha de Nacimiento</FormLabel>
                        <Input 
                          placeholder=''
                          type="date"
                          variant='outline' 
                          border='1px' 
                          borderColor='gray.500'
                          name="birdDate"
                          value={ form.birthday }
                          onChange={ ({ target }) => onChange(target.value, 'birthday') } 
                          autoComplete="off"
                        />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>Tipo de Sangre</FormLabel>
                        <Select
                          placeholder=''
                          variant='outline' 
                          border='1px' 
                          borderColor='gray.500'
                          name="blood"
                          value={form.blood}
                          onChange={ ({ target }) => onChange(target.value, 'blood') } 
                          autoComplete="off" 
                        >
                            <option value=''></option>
                            <option value='A+'>A+</option>
                            <option value='A-'>A-</option>
                            <option value='B+'>B+</option>
                            <option value='B-'>B-</option>
                            <option value='AB+'>AB+</option>
                            <option value='AB-'>AB-</option>
                            <option value='O+'>O+</option>
                            <option value='O-'>O-</option>
                        </Select>
                   {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>Peso al Nacer</FormLabel>
                        <InputGroup>
                          <InputLeftAddon children='Lbs' bgColor='gray.400' />
                          <NumberInput 
                            // defaultValue={0} 
                            value={form.weight}
                            precision={1} 
                            step={1}
                            onChange={ (e) => onChange(e, 'weight')}
                            // border='1px' 
                          >
                            <NumberInputField border='1px' borderColor='gray.500'  />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </InputGroup>

                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>Talla al Nacer</FormLabel>
                        <InputGroup>
                          <InputLeftAddon children='cm' bgColor='gray.400' />
                          <NumberInput 
                            // defaultValue={0} 
                            value={form.size_birth}
                            precision={2} 
                            step={0.1}
                            onChange={ (e) => onChange(e, 'size_birth')}
                            // border='1px' 
                          >
                            <NumberInputField border='1px' borderColor='gray.500'  />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </InputGroup>
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>Perimetro Cefalico</FormLabel>
                        <InputGroup>
                          <InputLeftAddon children='cm' bgColor='gray.400'  />
                          <NumberInput 
                            // defaultValue={0} 
                            value={form.head_circunference}
                            precision={2} 
                            step={0.1}
                            onChange={ (e) => onChange(e, 'head_circunference')}
                            // border='1px' 
                          >
                            <NumberInputField border='1px' borderColor='gray.500'  />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </InputGroup>
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>A Pagar</FormLabel>
                        <Input 
                          placeholder=''
                          variant='outline' 
                          border='1px' 
                          borderColor='gray.500'
                          name="toPay"
                          value={ form.a_pagar }
                          onChange={ ({ target }) => onChange(target.value, 'a_pagar') } 
                          autoComplete="off"
                        />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>Sexo</FormLabel>
                        <Select
                          placeholder=''
                          variant='outline' 
                          border='1px' 
                          borderColor='gray.500'
                          name="sex"
                          value={form.sex}
                          onChange={ ({ target }) => onChange(target.value, 'sex') } 
                          autoComplete="off" 
                        >
                          <option value=''></option>
                          <option value='masculino'>Masculino</option>
                          <option value='femenino'>Femenino</option>
                        </Select>
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                    <GridItem colSpan={1} >
                      <FormControl>
                        <FormLabel color='#31608C'>E.G</FormLabel>
                        <InputGroup>
                          <InputLeftAddon children='cm' bgColor='gray.400' />
                          <NumberInput 
                            // defaultValue={0} 
                            value={form.eg}
                            precision={2} 
                            step={0.1}
                            onChange={ (e) => onChange(e, 'eg')}
                            // border='1px' 
                          >
                            <NumberInputField border='1px' borderColor='gray.500'  />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </InputGroup>
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>
                  </SimpleGrid>
                </VStack>
              </Box>
              <Historial 
                patientId={ form._id}
                appoitments={ appointments }
                loadAppointments={ loadAppointments }
              />
            
            </HStack>
            <Flex w='100%' h='180px' mt={3} bgColor='#F5F5F5' borderRadius={8} >
              <Box w='70%'>
                <SimpleGrid columns={2} columnGap={3}  w='full' ml={3} px={5} pt={3} gridColumnGap={50} >
                    <GridItem colSpan={1} >
                      <FormControl>
                          <FormLabel color='#31608C'>Madre</FormLabel>
                          <Input 
                            placeholder=''
                            variant='outline' 
                            border='1px' 
                            borderColor='gray.500'
                            name="mother"
                            value={ form.motherName }
                            onChange={ ({ target }) => onChange(target.value, 'motherName') } 
                            autoComplete="off"
                          />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>   
                    <GridItem colSpan={1} >
                      <FormControl>
                          <FormLabel color='#31608C'>Padre</FormLabel>
                          <Input 
                            placeholder=''
                            variant='outline' 
                            border='1px' 
                            borderColor='gray.500'
                            name="father"
                            value={ form.fatherName }
                            onChange={ ({ target }) => onChange(target.value, 'fatherName') } 
                            autoComplete="off"
                          />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>   
                    <GridItem colSpan={1} >
                      <FormControl>
                          <FormLabel color='#31608C'>Telefono Madre</FormLabel>
                          <Input 
                            placeholder=''
                            variant='outline' 
                            border='1px' 
                            borderColor='gray.500'
                            name="momNumber"
                            value={ form.motherPhone }
                            onChange={ ({ target }) => onChange(target.value, 'motherPhone') } 
                            autoComplete="off"
                          />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>  
                    <GridItem colSpan={1} >
                      <FormControl>
                          <FormLabel color='#31608C'>Telefono Padre</FormLabel>
                          <Input 
                            placeholder=''
                            variant='outline' 
                            border='1px' 
                            borderColor='gray.500'
                            name="fatherNumber"
                            value={ form.fatherPhone }
                            onChange={ ({ target }) => onChange(target.value, 'fatherPhone') } 
                            autoComplete="off"
                          />
                            {/* {  !isTitleValid && (
                                  <FormErrorMessage>
                                      { formErrors.title }
                                  </FormErrorMessage>
                              )
                          } */}
                      </FormControl>
                    </GridItem>    
                </SimpleGrid>
              </Box>
              <Flex w='30%' alignItems='center' ml={10} >
                {
                  !isSearching && 
                  (
                    <Button onClick={ onSubmit } colorScheme="blue">Guardar</Button>
                  )
                }
              </Flex>
            </Flex>
          </Box>
        {/* </form> */}
      </Container>
    </>
  )
}

export default App